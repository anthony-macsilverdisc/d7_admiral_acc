<?php

/**
 * Implements hook_preprocess_page().
 */
function admiral_preprocess_page(&$variables)
{
  ///Load related blog posts into a region relative to a node id and multiple terms
  if (isset($variables['node'])) {
    $node_id = $variables['node']->nid;
    $language = $variables['node']->language;
    $bundle_type = $variables['page']['content']['system_main']['nodes'][$node_id]['#bundle'];
    if ($bundle_type == 'blog_post') {
      if (isset($variables['node']->field_blog_category[$language]['0']['tid'])) {
        $argument = '';
        foreach ($variables['node']->field_blog_category[$language] as $term) {
          $argument .= $term['tid'] . ',';
        }
        $argument = rtrim($argument, ",");

        //Don't load the view if there are no blog posts related
        //Use EFQ to check

        $content = views_embed_view('blog_listing', 'block_related_blog_posts', $argument);

        $variables['page']['blogrelated'] = $content;
      }
    }
  }

  //Checkout complete
  //Add in Google Analytics code for Checkout Complete
  if (arg(2) == 'complete' && arg(0) == 'checkout') {
    $order_id = arg(1);

    //Load order
    if (is_numeric(arg(1))) {
      //Check if we have an order number
      $order = commerce_order_load(arg(1));


      // Get the order wrapper
      $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

      $order_total = $order_wrapper->commerce_order_total->value();

      // Get ex vat amount. AS an array
      $total_ex_vat = commerce_price_component_total($order_total, 'base_price');

      $formatted_total_ex_vat = $total_ex_vat['amount'] / 100;


      //$currency_code = $wrapper->commerce_order_total->currency_code->value();

      $conversion_code = '
<!-- Google Code for Charity Card Sale Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 966698049;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "9Jr4CO-W5VYQwcj6zAM";
var google_conversion_value = ' . $formatted_total_ex_vat . ';
var google_conversion_currency = "GBP";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/966698049/?value=1.00&amp;currency_code=GBP&amp;label=9Jr4CO-W5VYQwcj6zAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>';
      $variables['page']['footer_2']['admiral_ga']['#markup'] = $conversion_code;
      $variables['page']['footer_2']['admiral_ga']['#prefix'] = '<div style="display: none">';
      $variables['page']['footer_2']['admiral_ga']['#suffix'] = '</div>';
      $variables['page']['footer_2']['#sorted'] = false;
    }

  }


  //Add Analytics code to Catalogue confirmation
  if(isset($variables['node'])) {
    if ($variables['node']->nid == '427') {
      $cat_confirm_code = '
<!-- Google Code for Catalogue Request Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 966698049;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "_QRVCJbGwlcQwcj6zAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/966698049/?label=_QRVCJbGwlcQwcj6zAM&guid=ON&script=0"/>
</div>
</noscript>';

      $variables['page']['footer_2']['admiral_ga']['#markup'] = $cat_confirm_code;
      $variables['page']['footer_2']['admiral_ga']['#prefix'] = '<div style="display: none">';
      $variables['page']['footer_2']['admiral_ga']['#suffix'] = '</div>';
      $variables['page']['footer_2']['#sorted'] = false;
    }
  }

}