<?php
/**
 * Implementation of hook_rules_action_info()
 */
function admiral_vat_rules_action_info(){
    return array(
        'admiral_vat_apply_vat' => array( // This is the name of the callback function
            'label' => t('Apply 20% VAT'),
            'group' => t('Commerce Line Item'),
            'parameter' => array(
                'line_item' => array(
                    'type' => 'commerce_line_item',
                    'label' => t('Line Item'),
                    'description' => t('The line item on which to apply the volume discount.'),
                    'wrapped' => true, // This is nice, it will be wrapped for us
                    'save' => true,    // And saved too!
                ),
                'vat_rate' => array(
                    'type' => 'decimal',
                    'label' => t('VAT Rate'),
                    'description' => t('Specify vat rate as a number. For example enter 20 for 20%'),
                ),
            ),
        ),
    );
}


/**
 * Rules action that applies a volume discount to an order
 * @param commerce_line_item $line_item
 * @return type
 */
function admiral_vat_apply_vat($line_item_wrapper, $vat_rate){
    if($line_item_wrapper->value()->type == 'ajw_product_attributes_line_item_custom'){

        // apply discount before calculating total price.
        $order = commerce_order_load($line_item_wrapper->order_id->value());
        $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
        $count = $order_wrapper->commerce_discounts->count();

        // Get the current calculated price of the product item
        $price = $line_item_wrapper->commerce_unit_price->value();
        $calculated_price = commerce_price_component_total($price);
        $qty = $line_item_wrapper->quantity->value();
        $total_line_item_price = $calculated_price['amount'] * $qty;

        $t = $order_wrapper->commerce_discounts->value();

        $discount_rates = 1;

        // commerce discount module
        if( $order_wrapper->__isset('commerce_discounts') ) {

            $discount_rates = 0;
            $discount_flat_amount = 0;
            //$types = commerce_discount_types();
            // $offer_types = commerce_discount_offer_types();
            foreach (entity_load('commerce_discount') as $discount) {

                if (commerce_coupon_order_has_discount($order, $discount)) {

                    $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount);
                    $offer_wrapper = $discount_wrapper->commerce_discount_offer;
                    $offer_type = $offer_wrapper->type->value();
                    $discount_type = $discount_wrapper->type->value();

                    // Savings value implementations on behalf of commerce discount.
                    switch ($discount_type) {
                        case 'order_discount':
                            switch ($offer_type) {
                                case 'fixed_amount':
                                    $discount_fixed_amount = $offer_wrapper->commerce_fixed_amount->value();

                                    break;
                                case 'percentage':
                                    $rate = $offer_wrapper->commerce_percentage->value();
                                    if ($rate < 1) {
                                        $rate = $rate * 100;
                                    }

                                    break;
                            }
                    }



                    if( !isset($discount_fixed_amount) ) {
                        $discount_fixed_amount = 0;
                    }

                    $discount_flat_amount += $discount_fixed_amount['amount'];

                    if(isset($rate)) {
                        if ($rate > 1) {
                            $rate = $rate / 100;
                        }

                        $discount_id = $discount_wrapper->discount_id->value();
                        $applied_discounts[$discount_id] = $rate;
                        $discount_rates += $rate;
                        $rate = 0;
                    }
                }
            }

            // fixed discount
            $vat_after_discount_amount_fixed_price = $total_line_item_price - $discount_flat_amount;

            // percentage discount
            $vat_after_discount_amount_percentage_price = $vat_after_discount_amount_fixed_price * ($discount_rates);

            // total discount
            $total_after_discounts_amount = $vat_after_discount_amount_fixed_price - $vat_after_discount_amount_percentage_price;

            $vat_amount = ( $total_after_discounts_amount * ( $vat_rate / 100 ) ) / $qty;
        }
        if($vat_amount > 0) {
            // Add the discount as a price component
            $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
                $line_item_wrapper->commerce_unit_price->value(),
                'admiral_vat',
                array(
                    'amount' => $vat_amount,
                    'currency_code' => 'GBP',
                    'data' => array()
                ),
                0 // NOT included already in the price
            );
        }
    }
}


