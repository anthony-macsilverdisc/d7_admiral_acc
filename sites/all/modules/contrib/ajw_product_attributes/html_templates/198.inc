<body>
<div id="left-panel" style="padding-top:377px;height:2474px;width:50%;float:left;">
    <div id="left-panel-image">198
        <? if (isset($left_panel_image) && $left_panel_image != '') { ?>
        <div style="width:1204px; height:1285px; margin-left:auto;margin-right:auto;text-align:center;">
            <?php echo $left_panel_image ?></div>
        <? } ?>
    </div>
</div>
<div id="right-panel" style="padding-top: 380px;height:2474px;width: 49%; float: left;background:white">
    <div id="greeting" style="height:774px;width: 1506px; margin-left: auto; margin-right: auto; background:white;">
        <?php echo $greeting ?>
    </div>
    <div id="signature" style="text-align:center;height:474px; width: 1204px; margin-left: auto; margin-right: auto; margin-top: 62px; background:white;">
        <? if ($signature_image != '') { ?>
            <?php echo $signature_image ?>
        <? } ?>
    </div>
    <div id="centre_bottom" style="height:377px;width: 599px; margin-left: auto; margin-right: auto; margin-top: 31px; background:white;">
        <? if ($centre_bottom != '') { ?>
            <?php echo $centre_bottom ?>
        <? } ?>
    </div>
    <div id="bottom-wrapper" style="width:1740px;max-height:483px;margin-left:auto;margin-right:auto;text-align:center;background:white">
            <div id="address" style="height:483px;width:312px;margin-left:auto;margin-right:auto;background:white">
                <?php echo $address ?>
            </div>
    </div>
</div>
</body>
