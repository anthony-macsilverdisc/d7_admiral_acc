<?php
/**
* @file
* This module allows submitting form data to SilverDisc's Form Processor system.
* For a form to be compatible with Form Procsesor it must:
*   - Have the following fields: contact, email, phone
*/

/**
 * 
 * Compute a hash that maps component keys to component key names.
 * @param DrupalNode $node
 * 
 * @return
 * Hash mapping key values to key names.
 */
function _formproc_webform_component_mapping($node) {
	$mapping = array();
	$wfComponents = $node->webform['components'];
	if (is_array($wfComponents)) {
		foreach ($wfComponents as $i => $components) {
			if (isset($components['form_key'])) {
				$key = $components['form_key'];
			} else {
				$key = 'component_' . $i;
			}
			$mapping[$key] = $components;
		}
	}
	return $mapping;
} 

/**
 * Implementation of hook_menu().
 */
function formproc_menu() {

	$items = array();
	$items['admin/settings/formproc'] = array(
		'title' 			=> t('Form Processor Connection Settings'),
		'description'		=> t("Settings needed to POST data to SilverDisc's Form Processor system."),
		'page callback' 	=> 'drupal_get_form',
		'page arguments' 	=> array('formproc_admin_settings'),
		'access arguments'	=> array('administer site configuration'),
		'type'				=> MENU_NORMAL_ITEM,
		'file'				=> 'formproc.admin.inc',
	);
	
	return $items;
}


/**
 * 
 * Posts the data to the endpoint.
 * 
 * @param string $url
 * the endpoint where data will be posted to.
 * 
 * @param arrayHash $paramsHash
 * the hash of variable names and values that will be posted to the endpoint.
 * 
 */
function _formproc_send_post($url, $paramsHash){

/*
	$serialisedParams= http_build_query($paramsHash);
	debug($url);
	debug($paramsHash, NULL, true);
	debug($serialisedParams);
	die();
*/

	$response= FALSE;
	$ch = curl_init($url);
	if ($ch) {
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($paramsHash));
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_HEADER,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);  // RETURN THE CONTENTS OF THE CALL
		$response = curl_exec($ch);
		curl_close($ch);
		if ($response === FALSE) {
			$msg = t('cURL has failed to POST form data.');
			// debug($msg);
			watchdog('formproc', $msg, NULL, WATCHDOG_ERROR);
		}
		
	} else {
		$msg = t('Not possible to instantiate cURL object to POST form data.');
		// debug($msg);
		watchdog('formproc', $msg, NULL, WATCHDOG_ERROR);
		
	}

	return $response;
}

/**
 * 
 * Check if submission array complies with Form Processor requirements.
 * 
 * @param array $toPost
 * key/value paire to submit.
 * 
 * @param string $msg
 * Error messages to report from analysing $toPost
 */
function _formproc_check_compliance(&$toPost, &$msg) {
	$minFieldKeys= variable_get('formproc_minimum_fields', 'contact,phone,email,siteId');
	$minFieldKeys= str_replace(' ', '', $minFieldKeys);
	$altKeys  = array ('contact' => array ('first_name', 'surname'));
	$checkKeys= explode(',', $minFieldKeys);
	// debug($minFieldKeys, NULL, true);
	// debug($checkKeys, NULL, true);
	$notFound= array();
	if (is_array($toPost)) {
		foreach ($checkKeys as $key) {
			if (!isset($toPost[$key])) {
				// See if there is an alternate configuration for this key.
				if (isset($altKeys[$key])) {
					$altError= false;
					$altValues= array();
					foreach ($altKeys[$key] as $altKey) {
						if (!isset($toPost[$altKey])) {
							$msg.= t('The form has no field with key "%theKey"', array('%theKey' => $altKey)) . ".\n";
							$altError= true;
						} else {
							$altValues[]= $toPost[$altKey];
						}
					}
					
					if (!$altError) {
						$toPost[$key]= implode(' ', $altValues);
					}
					
				} else {
					$msg.= t('The form has no field with key "%theKey"', array('%theKey' => $key)) . ".\n";
				}
			}
		}
	}
	
	if (!empty($msg)) {
		$msg= t('The form does not comply with Form Processor requirements.') . ' ' . $msg;
	}
	$r= empty($msg);
	return $r;
}

/**
* Implements hook_webform_submission_insert().
*
*/
function formproc_webform_submission_insert($node, $submission) {
	
	global $base_url;

	$nodeIdsToPost= str_replace(' ', '', variable_get('formproc_node_ids_to_post', ''));
	$nodeIdArray= explode(',', $nodeIdsToPost);
	
	$r = FALSE;
	
	if (!is_array($nodeIdArray)) {
		return $r;
	}
	
	if (!in_array($node->nid, $nodeIdArray)) {
		return $r;
	}
	
	$submissionUrl= variable_get('formproc_endpoint', 'http://fp.silverdisc.cl/processForm.php');
	$mapping = _formproc_webform_component_mapping($node);
	// debug($submission, NULL, true);
	// debug($mapping, NULL, true);

	if (is_array($mapping)) {
		$toPost= array();
		foreach ($mapping as $machineName => $components) {
			$componentIndex = $components['cid'];
			$componentType = $components['type'];
			if ($machineName == 'fp_endpoint') {
				if (!empty($submission->data[$componentIndex]['value'][0])) {
					$submissionUrl = $submission->data[$componentIndex]['value'][0];
				}
			
			} else if (isset($submission->data[$componentIndex])) {
				// TODO: Add support for D7 file type fields.
				if ($componentType == 'multifile') {
					$fileIdArray = unserialize($submission->data[$componentIndex]['value'][0]);
					$filesData = array();
					if (is_array($fileIdArray)) {
						foreach ($fileIdArray as $fid) {
							$fileRecord= db_query('SELECT * FROM {file_managed} WHERE fid = :fid', 
												  array(':fid' => $fid))->fetchObject();
							if ($fileRecord) {
								$fileRecordArray = (array) $fileRecord;
								$fileRecordArray['uri'] = file_create_url($fileRecord->uri);
								$filesData[] = $fileRecordArray;
							}
						}
					}
					$toPost[$machineName] = $filesData;

				} else if (isset($submission->data[$componentIndex]['value'][0])) {
					$toPost[$machineName]= $submission->data[$componentIndex]['value'][0];

				} else {
					$toPost[$machineName]= '';

				}
			}
		}
		$toPost['siteurl'] = $base_url;
		
		// debug($toPost, NULL, true);
		$msg= '';
		if (_formproc_check_compliance($toPost, $msg)) {
			$r = _formproc_send_post($submissionUrl, $toPost);
		} else {
			debug(t('Form not submitted to Form Processor. Please inspect the Drupal log to find out the cause.'));
			watchdog('formproc', 'formproc_webform_submission_insert: ' . $msg, NULL, WATCHDOG_ERROR);
		}
		
	} else {
		$msg = t('Not possible to compute data mapping structure.');
		// debug($msg);
		watchdog('formproc', 'formproc_webform_submission_insert: ' . $msg, NULL, WATCHDOG_ERROR);
		
	}
	
	return $r;
}

/**
* Implements hook_help().
*
* Displays help and module information.
*
* @param path
*   Which path of the site we're using to display help
* @param arg
*   Array that holds the current path as returned from arg() function
*/
function formproc_help($path, $arg) {
  switch ($path) {
    case "admin/help#formproc":
    	$helpText = '<p>' . t("This module submits form data to SilverDisc's Form Processor System.") . '</p>';
    	$helpText.= '<p>' . t("Form Processor needs every form to contain at least the following four fields with exactly these field keys:") . '</p>';
    	$helpText.= '<ul>';
    	$helpText.= '<li>' . t("contact and/or first_name + surname: the user making the contact. If there is no 'contact' field, " . 
    							"the system will require both the first_name and surname fields, and will concatenate them into a 'contact' key.") . '</li>';
    	$helpText.= '<li>' . t("email: the user's email address.") . '</li>';
    	$helpText.= '<li>' . t("phone: the user's phone number.") . '</li>';
    	$helpText.= '<li>' . t("siteId (hidden): the Form Processor id of this form.") . '</li>';
    	$helpText.= '</ul>';
    	$helpText.= '<p>' . t("The keys sitename and siteurl are reserved for internal use. " .
    						  "Do not use these field keys in your webform if you are using this module.") . '</p>';
       	return $helpText;
      break;
  }
}
